window.addEventListener("load", function() {
	// Stran naložena

	// Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML, 10);
			
			if(cas != 0){
				cas = cas-1;
				casovnik.innerHTML=cas;
			}
			else {
				alert("Opomnik!\n\nZadolžitev "+ opomnik.querySelector(".naziv_opomnika").innerHTML +" je potekla!");
				document.querySelector("#opomniki").removeChild(opomnik);
			}
	
            
			// TODO:
			// - če je čas enak 0, izpiši opozorilo
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	};

	setInterval(posodobiOpomnike, 1000);
	var obKlikuNaGumb = function() {
		var ime = document.querySelector("#uporabnisko_ime").value;
		document.getElementById("uporabnik").innerHTML = ime;
		document.querySelector(".pokrivalo").style.visibility = "hidden";

	};
	document.getElementById("prijavniGumb").addEventListener("click", obKlikuNaGumb);

	

	var dodajOpomnik = function() {
		var cas = document.querySelector("#cas_opomnika").value;
		document.querySelector("#cas_opomnika").value ="";
		var naziv = document.querySelector("#naziv_opomnika").value;
		document.querySelector("#naziv_opomnika").value ="";
		var opomniki = document.querySelector("#opomniki");
		
		opomniki.innerHTML +=" \
		<div class='opomnik senca rob'> \
            <div class='naziv_opomnika'>"+ naziv + "</div> \
            <div class='cas_opomnika'> Opomnik čez <span>" + cas + "</span> sekund.</div> \
        </div>";
	};
	document.querySelector("#dodajGumb").addEventListener("click", dodajOpomnik);
});
